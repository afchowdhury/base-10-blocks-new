﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Reset : MonoBehaviour
{
    public GameObject cloneContainer;
    public List<GameObject> valueContainers = new List<GameObject>();
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ResetGrid()
    {
        foreach(GameObject i in valueContainers)
        {
            i.GetComponent<InputField>().text = "";
        }

        foreach(Transform child in cloneContainer.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
