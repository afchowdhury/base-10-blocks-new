﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TwinklAuthenticationGateway;
using TwinklStandardClasses;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace TwinklGameManager
{
    /// <summary>
    /// Controls transition between Unity scenes.
    /// Various options for scene loading.
    /// </summary>
    public class ScreenModeController: Singleton<ScreenModeController>
    {
        private bool _fullScreen;

        public bool fullScreen
        {
            get { return _fullScreen;}
            set
            {
                if (_fullScreen != value)
                {
                    
                    PlayerPrefs.SetInt("FullScreen", value ? 1 : 0);
                    _fullScreen = value;
                    onScreenStateChanged?.Invoke(value);
                    updateScreenDisplay();
                }
            }
        }
        
        public BoolEvent onScreenStateChanged = new BoolEvent();
        
        protected override void Awake()
        {
            _fullScreen = PlayerPrefs.GetInt("FullScreen") == 0;
            updateScreenDisplay();
        }

        private void updateScreenDisplay()
        {
            
            Screen.fullScreenMode = fullScreen ? FullScreenMode.ExclusiveFullScreen : FullScreenMode.Windowed;
        }

    }
}