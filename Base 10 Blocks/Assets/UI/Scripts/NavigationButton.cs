using TwinklGameManager;
using UnityEngine;

namespace UI.Scripts
{
    
    
    public class NavigationButton: MonoBehaviour
    {
        public void SwitchScene(string MainGame)
        {
            SceneController.Instance.SwitchScene(MainGame);
        }

        /*public void AddScene(string _sceneName)
        {
            SceneController.Instance.AddScene(_sceneName);
        }*/
    }
}