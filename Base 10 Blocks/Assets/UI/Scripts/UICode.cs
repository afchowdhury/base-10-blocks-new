﻿using TwinklGameManager;
using UnityEngine;
using UnityEngine.UI;

public class UICode : MonoBehaviour
{
    [Header("Icons")]
    public Sprite muteOnIcon;
    public Sprite muteOffIcon;
    public Sprite expandOnIcon;
    public Sprite expandOffIcon;
    [Header("Button Image")]
    public Image muteButtonImage; 
    public Image expandButtonImage;
    [Header("Buttons")]
    public Button muteButton; 
    public Button expandButton;

    void Start()
    {
#if !UNITY_WEBGL
        muteButton.gameObject.SetActive(false);
        expandButton.gameObject.SetActive(false);
#endif
        setMuteStateIcon(AudioController.Instance.muted);
        AudioController.Instance.onMuteStateChanged.AddListener(setMuteStateIcon);
        setScreenSizeIcon(ScreenModeController.Instance.fullScreen);
        ScreenModeController.Instance.onScreenStateChanged.AddListener(setScreenSizeIcon);
        muteButton.onClick.AddListener(toggleSound);
        expandButton.onClick.AddListener(toggleFullScreen);
    }

    private void OnDestroy()
    {
        AudioController.Instance?.onMuteStateChanged.RemoveListener(setMuteStateIcon);
        ScreenModeController.Instance?.onScreenStateChanged.RemoveListener(setScreenSizeIcon);
    }

    private void EnableExpandButton()
    {
        muteButtonImage.gameObject.SetActive(false);
        expandButtonImage.gameObject.SetActive(false);
    }

    //Remember State of Mute Button When Changing Scenes
    private void toggleSound()
    {
        AudioController.Instance.muted = !AudioController.Instance.muted;
    }

    private void toggleFullScreen()
    {
        ScreenModeController.Instance.fullScreen = !ScreenModeController.Instance.fullScreen;
    }


    //Set The Sound Toggle State
    private void setMuteStateIcon(bool _muted)
    {
        muteButtonImage.sprite = _muted ? muteOffIcon : muteOnIcon;
    }

    //Set The Sound Toggle State
    private void setScreenSizeIcon(bool _fullScreen)
    {
        expandButtonImage.sprite = _fullScreen ? expandOffIcon : expandOnIcon;
    }
}